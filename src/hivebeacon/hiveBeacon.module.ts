import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { HiveBeaconService } from './hiveBeacon.service'

@Module({
  imports: [HttpModule],
  controllers: [],
  providers: [HiveBeaconService],
  exports: [HiveBeaconService]
})
export class HiveBeaconModule {}
