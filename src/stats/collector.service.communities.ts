import { Injectable, Logger } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'
import { HiveSqlService } from '../hivesql/hiveSql.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { SettingsService } from '../settings/settings.service'

@Injectable()
export class CollectorServiceCommunities {
  private readonly logger = new Logger(CollectorServiceCommunities.name)
  private settings = new SettingsService

  constructor(
    @InjectConnection('localConnection')
    private sequelize: Sequelize,
    private hiveSqlService: HiveSqlService,
    private localConnectionService: LocalConnectionService,
  ) {}
  
  /*
   * Collect daily communities stats
   */
  async collectDailyCommunitiesStats(today: string) {

    try {
      await this.collectDailyCommunitiesSubscribersStats(today, 'subscribers_change')
    } catch (e) {
      this.logger.error('Error collecting communities_subscribers_stats', e)
    }

    try {
      await this.collectDailyCommunitiesCommentStats(today, 'posts')
    } catch (e) {
      this.logger.error('Error collecting communities_post_stats', e)
    }

    try {
      await this.collectDailyCommunitiesCommentStats(today, 'comments')
    } catch (e) {
      this.logger.error('Error collecting communities_comment_stats', e)
    }

  }

  /*
   * Collect daily communities subscribers stats
   */
  async collectDailyCommunitiesSubscribersStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_communities_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    const communities = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, JSON_VALUE(json,'$[1].community') AS community,
    COUNT(
      CASE
        WHEN JSON_VALUE(json,'$[0]') = 'subscribe' THEN 1 ELSE NULL
      END) AS subscribers,
    COUNT(
      CASE
        WHEN JSON_VALUE(json,'$[0]') = 'unsubscribe' THEN 1 ELSE NULL
      END) AS unsubscribers
    FROM TxCustoms
    WHERE tid = 'community'
    AND ISJSON(json) > 0
    AND TRIM(JSON_VALUE(json,'$[1].community')) LIKE 'hive-%'
    AND JSON_VALUE(json,'$[0]') IN('subscribe','unsubscribe')
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp), JSON_VALUE(json,'$[1].community')
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr:today}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_daily (day, community, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [community.year, community.month, community.day, community.community, metricName, (community.subscribers - community.unsubscribers)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily communities post and comment stats
   */
  async collectDailyCommunitiesCommentStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_communities_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    let operator
    if(metricName == 'posts') operator = '='
    if(metricName == 'comments') operator = '<>'

    const communities = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, category, COUNT(*) AS count
    FROM Comments
    WHERE category LIKE 'hive-[0-9][0-9][0-9][0-9][0-9]%'
    AND depth ${operator} 0
    AND created > :lastDateStr
    AND created < :todayStr
    GROUP BY YEAR(created), MONTH(created), DAY(created), category
    ORDER BY year, month, day, count DESC;`,
    {lastDateStr: lastDate, todayStr:today}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_daily (day, community, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [community.year, community.month, community.day, community.category, metricName, community.count],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily communities metadata
   */
  async collectCommunitiesMeta() {
    
    /* The metadata for communities can always change, so we just delete and repopulate what we have */
    
    const communities = await this.hiveSqlService.executeQuery(
    `SELECT *
    FROM Communities;`,
    {}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      await this.localConnectionService.executeTranQuery(
        `DELETE FROM hive_communities_meta;`,
        [],
        tr
      )
      
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_meta (community_id, type, title, about, description, language, nsfw)
        VALUES (?, ?, ?, ?, ?, ?, ?);`,
        [community.name, community.type, community.title, community.about, community.description, community.language, community.nsfw],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

}
