import { Injectable } from '@nestjs/common'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { SettingsService } from '../settings/settings.service'

@Injectable()
export class StatsService {
  
  constructor(
    private localConnectionService: LocalConnectionService
    ) {}
    
  private settings = new SettingsService
  
  /*
   * Map time unit to Postgres date/time patterns
   * https://www.postgresql.org/docs/current/functions-formatting.html
   */
  async getUnit(time_unit: string): Promise<string> {
    switch(time_unit) {
      case 'daily':
        time_unit = 'day'
        break
      case 'weekly':
        time_unit = 'IYYY-IW'
        break
      case 'monthly':
        time_unit = 'YYYY-MM'
        break
      case 'yearly':
        time_unit = 'YYYY'
        break
      case 'lifetime':
        time_unit = 'AD'
        break
      default:
        time_unit = 'day'
    }
    return time_unit
  }

  /*
   * Apply any additional formatting needed to the SQL query. This is done here instead of in the query itself to avoid the extra complexity it would add, especially if the formatting has to be done conditionally.
   */
  async formatQuery(query: string, time_unit: string): Promise<string> {
    
    //Output the first date of each week, instead of the year-week format (e.g. 2022-53)
    if(time_unit == 'IYYY-IW') {
      query = query.replaceAll( "to_char(", "to_date(to_char(" )
      query = query.replaceAll( "day, :unitStr)", "day, :unitStr), :unitStr)" )
    }
    return query
  }
  
  async exchanges(): Promise<any> {
    
    let resultsObj = {}

    //Get all exchanges and their associated Hive accounts
    for(var exchangeName in this.settings.exchanges) {
      let assocHiveAccounts = this.settings.exchanges[exchangeName]
      resultsObj[exchangeName] = assocHiveAccounts
    }
    
    return resultsObj
  }
  
  async metrics(): Promise<any> {
    
    var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT key, value
      FROM text_strings
      ORDER BY key ASC;`,
      []
      )

    //Instead of an array of objects, make it an object with elements
    let resultsObj = {}
    results.forEach(async (result) => {
      resultsObj[result.key] = {desc: result.value}
    })
    
    return resultsObj
  }

  async trackedApps(): Promise<any> {

    let resultsObj = {}

    //Get all apps and their associated custom_jsons
    for(var app of this.settings.trackedApps) {
      for(var appName in app) {
        let assocCustomJsons = app[appName]
        resultsObj[appName] = this.settings[assocCustomJsons]
      }
    }
    
    return resultsObj
  }

  async getComments(type: string, time_unit: string, query): Promise<number[]> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT day AS unit, total_num
      FROM hive_comments_daily
      WHERE type = ? 
      ORDER BY unit DESC
      LIMIT ? OFFSET ?;`,
      [type, query.limit, query.offset]
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT to_char(day, :unitStr) AS unit, SUM(total_num) AS total_num
      FROM hive_comments_daily
      WHERE type = :typeStr
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, typeStr: type, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }
  
  async getTransfers(metricName: string, time_unit: string, query): Promise<number[]> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    if(metricName == 'DHF_proposal_payments' && time_unit != 'day') {
      return await this.getTransfersUnique(metricName, time_unit, query)
    }
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT day AS unit, num, TRUNC(amount / 1000.000, 3) AS amount
      FROM hive_transfers_daily
      WHERE metric_name = :metricStr
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`,
      {metricStr: metricName, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT to_char(day, :unitStr) AS unit, SUM(num) AS num, TRUNC(SUM(amount / 1000.000), 3) AS amount
      FROM hive_transfers_daily
      WHERE metric_name = :metricStr
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, metricStr: metricName, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }
  
  /* Unique transfers such as number of unique proposals funded in a period larger than day */
  async getTransfersUnique(metricName: string, time_unit: string, query): Promise<number[]> {
    
    if(time_unit == 'IYYY-IW') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT to_date(time_unit, 'IYYY-IW') AS unit, num, TRUNC(amount / 1000.000, 3) AS amount
       FROM hive_transfers
       WHERE metric_name = 'DHF_proposal_payments_weekly'
       order by unit DESC
       LIMIT :limitStr OFFSET :offsetStr;`,
      {limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else if(time_unit == 'YYYY-MM') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT time_unit AS unit, num, TRUNC(amount / 1000.000, 3) AS amount
       FROM hive_transfers
       WHERE metric_name = 'DHF_proposal_payments_monthly'
       order by (regexp_split_to_array(time_unit, '-'))[1]::integer DESC, (regexp_split_to_array(time_unit, '-'))[2]::integer DESC
       LIMIT :limitStr OFFSET :offsetStr;`,
       {limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else if(time_unit == 'YYYY') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT time_unit AS unit, num, TRUNC(amount / 1000.000, 3) AS amount
       FROM hive_transfers
       WHERE metric_name = 'DHF_proposal_payments_yearly'
       order by time_unit::integer DESC
       LIMIT :limitStr OFFSET :offsetStr;`,
       {limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else if(time_unit == 'AD') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT time_unit AS unit, num, TRUNC(amount / 1000.000, 3) AS amount
       FROM hive_transfers
       WHERE metric_name = 'DHF_proposal_payments_lifetime'
       LIMIT :limitStr OFFSET :offsetStr;`,
       {limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }
  
  async getRC(metricName: string, time_unit: string, query): Promise<number[]> {
    let rcMetricName = 'rc_' + metricName
    let hpMetricName = 'hp_equiv_' + metricName.replace('cost_', '')
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT rc.day AS unit, rc.value AS rc_cost, hp.value AS hp_equiv
      FROM (
        SELECT day, metric_name, value 
        FROM hive_rc_daily
        WHERE metric_name = :rcMetricStr) rc
      FULL JOIN (
        SELECT day, metric_name, TRUNC(value / 10000.000, 4) AS value 
        FROM hive_rc_daily
        WHERE metric_name = :hpMetricStr) hp
      ON rc.day = hp.day
      AND REPLACE(rc.metric_name,'rc_cost_','') = REPLACE(hp.metric_name,'hp_equiv_','')
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`,
      {rcMetricStr: rcMetricName, hpMetricStr: hpMetricName, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT to_char(rc.day, :unitStr) AS unit, TRUNC(AVG(rc.value)) as average_rc_cost, TRUNC(AVG(hp.value), 4) as average_hp_equiv
      FROM (
        SELECT day, metric_name, value 
        FROM hive_rc_daily
        WHERE metric_name = :rcMetricStr) rc
      FULL JOIN (
        SELECT day, metric_name, TRUNC(value / 10000.000, 4) AS value 
        FROM hive_rc_daily
        WHERE metric_name = :hpMetricStr) hp
      ON rc.day = hp.day
      AND REPLACE(rc.metric_name,'rc_cost_','') = REPLACE(hp.metric_name,'hp_equiv_','')
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, rcMetricStr: rcMetricName, hpMetricStr: hpMetricName, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }
  
  async getCustomJsons(outputType: string, appName: string, time_unit: string, query): Promise<any> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    let colName = ''
    let colNameOutput = ''
    let appList = []
    
    if(outputType == 'apps') {
      colName = 'app'
      colNameOutput = 'app'
    }
    else if(outputType == 'custom_jsons') {
      colName = 'metric_name'
      colNameOutput = 'custom_json'
    }

    if(appName == 'all_apps') {
      for(var app of this.settings.trackedApps) {
        for(var name in app) {
          appList.push(name)
        }
      }
    }
    else {
      appList.push(appName)
    }
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT day AS unit,
      CASE
          WHEN GROUPING(${colName}) = 1 THEN 'Total'
          ELSE ${colName}
      END ${colNameOutput},
      SUM(value) AS value
      FROM hive_operations_daily
      WHERE app IN(?)
      GROUP BY 
        ROLLUP(unit, ${colName})
      ORDER BY unit DESC, GROUPING(${colName}), ${colName} ASC
      LIMIT ? OFFSET ?;`,
      [appList, query.limit, query.offset]
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT to_char(day, :unitStr) AS unit,
      CASE
          WHEN GROUPING(${colName}) = 1 THEN 'Total'
          ELSE ${colName}
      END ${colNameOutput}, 
      SUM(value) AS value
      FROM hive_operations_daily
      WHERE app IN(:appListStr)
      GROUP BY 
        ROLLUP(unit, ${colName})
      ORDER BY unit DESC, GROUPING(${colName}), ${colName} ASC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, appListStr: appList, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    //Return the result as multiple data points nested under each day
    let resultsObj = {}
    results.forEach(async (result) => {
      if (typeof resultsObj[result.unit] === 'undefined') resultsObj[result.unit] = {}
      resultsObj[result.unit][result[colNameOutput]] = result.value
    })
    
    return resultsObj
  }

  async getMarket(metricName: string, time_unit: string, query): Promise<number[]> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT day AS unit, value
      FROM hive_market_daily
      WHERE metric_name = ?
      ORDER BY unit DESC
      LIMIT ? OFFSET ?;`,
      [metricName, query.limit, query.offset]
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT to_char(day, :unitStr) AS unit, TRUNC(AVG(value)) as average_value
      FROM hive_market_daily
      WHERE metric_name = :metricStr
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, metricStr: metricName, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }

  async getCommunity(community: string, metricName: string, time_unit: string, query): Promise<number[]> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT day AS unit, value, SUM(value) OVER(ORDER BY day RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumulative
      FROM hive_communities_daily
      WHERE community = ?
      AND metric_name = ?
      ORDER BY unit DESC
      LIMIT ? OFFSET ?;`,
      [community, metricName, query.limit, query.offset]
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT to_char(day, :unitStr) AS unit, SUM(value) AS value, SUM(SUM(value)) OVER(ORDER BY to_char(day, :unitStr) RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumulative
      FROM hive_communities_daily
      WHERE community = :communityStr
      AND metric_name = :metricStr
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, communityStr: community, metricStr: metricName, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }

  async getCommunitiesMeta(query): Promise<any> {
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT community_id, type, title, about, description, language, nsfw, d.value as total_subscribers
      FROM (
        SELECT *
        FROM hive_communities_meta
        WHERE nsfw = false
      ) m
      LEFT JOIN (
        SELECT community, SUM(value) AS value
        FROM hive_communities_daily
        WHERE metric_name = 'subscribers_change'
        GROUP BY community
      ) d
      ON m.community_id = d.community
      LIMIT ? OFFSET ?;`,
      [query.limit, query.offset]
      )

    //Instead of an array of objects, make it an object with elements
    let resultsObj = {}
    results.forEach(async (result) => {
      resultsObj[result.community_id] = {
        type: result.type,
        title: result.title,
        about: result.about,
        description: result.description,
        language: result.language,
        nsfw: result.nsfw,
        total_subscribers: result.total_subscribers
      }
    })
    
    return resultsObj
  }

  async getHbdInSavings(time_unit: string, query): Promise<number[]> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT 
        CASE
          WHEN t.day IS NOT NULL THEN t.day
          WHEN f.day IS NOT NULL THEN f.day
          WHEN i.day IS NOT NULL THEN i.day
        END AS unit,
      CASE WHEN t.num IS NULL THEN 0 ELSE t.num END AS num_transfers_to,
      CASE WHEN f.num IS NULL THEN 0 ELSE f.num END AS num_transfers_from,
      CASE WHEN i.num IS NULL THEN 0 ELSE i.num END AS num_interest_payouts,
      CASE WHEN t.amount IS NULL THEN 0 ELSE t.amount END AS amount_to,
      CASE WHEN i.amount IS NULL THEN 0 ELSE i.amount END AS interest_paid,
      CASE WHEN f.amount IS NULL THEN 0 ELSE f.amount END AS amount_from,
      (CASE WHEN t.amount IS NULL THEN 0 ELSE t.amount END + CASE WHEN i.amount IS NULL THEN 0 ELSE i.amount END - CASE WHEN f.amount IS NULL THEN 0 ELSE f.amount END) AS net_change,
      SUM(CASE WHEN t.amount IS NULL THEN 0 ELSE t.amount END + CASE WHEN i.amount IS NULL THEN 0 ELSE i.amount END - CASE WHEN f.amount IS NULL THEN 0 ELSE f.amount END) 
        OVER(ORDER BY 
          CASE 
            WHEN t.day IS NOT NULL THEN t.day
            WHEN f.day IS NOT NULL THEN f.day
            WHEN i.day IS NOT NULL THEN i.day
          END) AS cumulative
      FROM (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hbd_to_savings'
      ) t
      FULL JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hbd_from_savings'
      ) f
      ON t.day = f.day
      FULL JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'hbd_savings_interest_payments'
      ) i
      ON t.day = i.day
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`,
      {limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(`SELECT 
        CASE 
          WHEN to_char(t.day, :unitStr) IS NOT NULL THEN to_char(t.day, :unitStr)
          WHEN to_char(f.day, :unitStr) IS NOT NULL THEN to_char(f.day, :unitStr)
          WHEN to_char(i.day, :unitStr) IS NOT NULL THEN to_char(i.day, :unitStr)
        END AS unit,
      CASE WHEN SUM(t.num) IS NULL THEN 0 ELSE SUM(t.num) END AS num_transfers_to,
      CASE WHEN SUM(f.num) IS NULL THEN 0 ELSE SUM(f.num) END AS num_transfers_from,
      CASE WHEN SUM(i.num) IS NULL THEN 0 ELSE SUM(i.num) END AS num_interest_payouts,
      CASE WHEN SUM(t.amount) IS NULL THEN 0 ELSE SUM(t.amount) END AS amount_to,
      CASE WHEN SUM(i.amount) IS NULL THEN 0 ELSE SUM(i.amount) END AS interest_paid,
      CASE WHEN SUM(f.amount) IS NULL THEN 0 ELSE SUM(f.amount) END AS amount_from,
      (CASE WHEN SUM(t.amount) IS NULL THEN 0 ELSE SUM(t.amount) END + CASE WHEN SUM(i.amount) IS NULL THEN 0 ELSE SUM(i.amount) END - CASE WHEN SUM(f.amount) IS NULL THEN 0 ELSE SUM(f.amount) END) AS net_change,
      SUM(CASE WHEN SUM(t.amount) IS NULL THEN 0 ELSE SUM(t.amount) END + CASE WHEN SUM(i.amount) IS NULL THEN 0 ELSE SUM(i.amount) END - CASE WHEN SUM(f.amount) IS NULL THEN 0 ELSE SUM(f.amount) END) 
        OVER(ORDER BY 
          CASE 
            WHEN to_char(t.day, :unitStr) IS NOT NULL THEN to_char(t.day, :unitStr)
            WHEN to_char(f.day, :unitStr) IS NOT NULL THEN to_char(f.day, :unitStr)
            WHEN to_char(i.day, :unitStr) IS NOT NULL THEN to_char(i.day, :unitStr)
          END) AS cumulative
      FROM (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hbd_to_savings'
      ) t
      FULL JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hbd_from_savings'
      ) f
      ON t.day = f.day
      FULL JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'hbd_savings_interest_payments'
      ) i
      ON t.day = i.day
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }

  async getExchBalance(exchange: string, token: string, time_unit: string, query): Promise<any> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.offset === 'undefined') query.offset = 0
    query.limit = (typeof query.limit === 'undefined') ? 999999 : parseInt(query.offset) + parseInt(query.limit)

    let exchangesList = []
    let metricNamesTo = []
    let metricNamesFrom = []

    if(exchange == 'all_exchanges') {
      for(var exchangeName in this.settings.exchanges) {
        exchangesList.push(exchangeName)
      }
    }
    else {
      exchangesList.push(exchange)
    }

    for(var exch of exchangesList) {
      metricNamesTo.push('transfers_' + token.toLowerCase() + '_to_exchange__' + exch)
      metricNamesFrom.push('transfers_' + token.toLowerCase() + '_from_exchange__' + exch)
    }
    
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT unit, exchange, num_transfers_to, num_transfers_from, amount_to, amount_from, net_change, cumulative FROM (
      SELECT CASE WHEN t.day IS NULL THEN f.day ELSE t.day END AS unit,
      CASE WHEN t.metric_name IS NULL THEN f.metric_name ELSE t.metric_name END AS exchange,
      CASE WHEN t.num IS NULL THEN 0 ELSE t.num END AS num_transfers_to,
      CASE WHEN f.num IS NULL THEN 0 ELSE f.num END AS num_transfers_from,
      CASE WHEN t.amount IS NULL THEN 0 ELSE t.amount END AS amount_to,
      CASE WHEN f.amount IS NULL THEN 0 ELSE f.amount END AS amount_from,
      (CASE WHEN t.amount IS NULL THEN 0 ELSE t.amount END - CASE WHEN f.amount IS NULL THEN 0 ELSE f.amount END) AS net_change,
      SUM(CASE WHEN t.amount IS NULL THEN 0 ELSE t.amount END - CASE WHEN f.amount IS NULL THEN 0 ELSE f.amount END) 
        OVER(
          PARTITION BY CASE WHEN t.metric_name IS NULL THEN f.metric_name ELSE t.metric_name END
          ORDER BY CASE WHEN t.day IS NULL THEN f.day ELSE t.day END) AS cumulative,
      DENSE_RANK () OVER (ORDER BY CASE WHEN t.day IS NULL THEN f.day ELSE t.day END DESC) as rownum
      FROM (
        SELECT day, split_part(metric_name, '__', 2) AS metric_name, SUM(num) AS num, TRUNC(SUM(amount / 1000.000), 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name IN(:toStr)
        GROUP BY day, metric_name
        ORDER BY day, metric_name
      ) t
      FULL JOIN (
        SELECT day, split_part(metric_name, '__', 2) AS metric_name, SUM(num) AS num, TRUNC(SUM(amount / 1000.000), 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name IN(:fromStr)
        GROUP BY day, metric_name
        ORDER BY day, metric_name
      ) f
      ON t.day = f.day
      AND t.metric_name = f.metric_name
      ORDER BY unit DESC, exchange ASC) AS result
      WHERE rownum > :offsetStr
      AND rownum <= :limitStr;`,
      {toStr: metricNamesTo, fromStr: metricNamesFrom, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(
      `SELECT unit, exchange, num_transfers_to, num_transfers_from, amount_to, amount_from, net_change, cumulative FROM(
      SELECT CASE WHEN to_char(t.day, :unitStr) IS NULL THEN to_char(f.day, :unitStr) ELSE to_char(t.day, :unitStr) END AS unit,
      CASE WHEN t.metric_name IS NULL THEN f.metric_name ELSE t.metric_name END AS exchange,
      CASE WHEN SUM(t.num) IS NULL THEN 0 ELSE SUM(t.num) END AS num_transfers_to,
      CASE WHEN SUM(f.num) IS NULL THEN 0 ELSE SUM(f.num) END AS num_transfers_from,
      CASE WHEN SUM(t.amount) IS NULL THEN 0 ELSE SUM(t.amount) END AS amount_to,
      CASE WHEN SUM(f.amount) IS NULL THEN 0 ELSE SUM(f.amount) END AS amount_from,
      (CASE WHEN SUM(t.amount) IS NULL THEN 0 ELSE SUM(t.amount) END - CASE WHEN SUM(f.amount) IS NULL THEN 0 ELSE SUM(f.amount) END) AS net_change,
      SUM(CASE WHEN SUM(t.amount) IS NULL THEN 0 ELSE SUM(t.amount) END - CASE WHEN SUM(f.amount) IS NULL THEN 0 ELSE SUM(f.amount) END) 
        OVER(
          PARTITION BY CASE WHEN t.metric_name IS NULL THEN f.metric_name ELSE t.metric_name END
          ORDER BY CASE WHEN to_char(t.day, :unitStr) IS NULL THEN to_char(f.day, :unitStr) ELSE to_char(t.day, :unitStr) END) AS cumulative,
      DENSE_RANK () OVER (ORDER BY CASE WHEN to_char(t.day, :unitStr) IS NULL THEN to_char(f.day, :unitStr) ELSE to_char(t.day, :unitStr) END DESC) as rownum
      FROM (
        SELECT day, split_part(metric_name, '__', 2) AS metric_name, SUM(num) AS num, TRUNC(SUM(amount / 1000.000), 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name IN(:toStr)
        GROUP BY day, metric_name
        ORDER BY day, metric_name
      ) t
      FULL JOIN (
        SELECT day, split_part(metric_name, '__', 2) AS metric_name, SUM(num) AS num, TRUNC(SUM(amount / 1000.000), 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name IN(:fromStr)
        GROUP BY day, metric_name
        ORDER BY day, metric_name
      ) f
      ON t.day = f.day
      AND t.metric_name = f.metric_name
      GROUP BY unit, exchange
      ORDER BY unit DESC, exchange ASC) AS results
      WHERE rownum > :offsetStr
      AND rownum <= :limitStr;`, time_unit),
      {unitStr: time_unit, toStr: metricNamesTo, fromStr: metricNamesFrom, limitStr: query.limit, offsetStr: query.offset}
      )
    }

    if(exchange == 'all_exchanges') {
      //Return the result as multiple data points nested under each day and exchange
      let resultsObj = {}
      results.forEach(async (result) => {
        if (typeof resultsObj[result.unit] === 'undefined') resultsObj    [result.unit] = {}
        if (typeof resultsObj[result.unit][result.exchange] === 'undefined') resultsObj[result.unit][result.exchange] = {}
        resultsObj[result.unit][result.exchange].num_transfers_to = result.num_transfers_to
        resultsObj[result.unit][result.exchange].num_transfers_from = result.num_transfers_from
        resultsObj[result.unit][result.exchange].amount_to = result.amount_to
        resultsObj[result.unit][result.exchange].amount_from = result.amount_from
        resultsObj[result.unit][result.exchange].net_change = result.net_change
        resultsObj[result.unit][result.exchange].cumulative = result.cumulative
      })
      return resultsObj
    }
    else {
      return results
    }
  }

  async getDhfBalance(time_unit: string, query): Promise<number[]> {
    time_unit = await this.getUnit(time_unit)
    if (typeof query.limit === 'undefined') query.limit = null
    if (typeof query.offset === 'undefined') query.offset = 0
    
    //New emissions happen reliably every day, so we can left join all to that table  
    if(time_unit == 'day') {
      var [results, metadata] = await this.localConnectionService.executeQuery(
      `SELECT ehbd.day AS unit,
      COALESCE(phivein.amount, 0) AS premine_hive_in,
      COALESCE(phbdout.amount, 0) AS premine_hbd_out,
      COALESCE(thivein.num, 0) AS transfers_hive_num,
      COALESCE(thivein.amount, 0) AS transfers_hive_amount,
      COALESCE(thbdout.amount, 0) AS transfers_hive_amount_hbd_out,
      COALESCE(thbd.num, 0) AS transfers_hbd_num,
      COALESCE(thbd.amount, 0) AS transfers_hbd_amount,
      COALESCE(bhbd.num, 0) AS beneficiary_rewards_num,
      COALESCE(bhbd.amount, 0) AS beneficiary_rewards_hbd_amount,
      COALESCE(ehbd.amount, 0) AS new_emission_allocation_hbd_amount,
      COALESCE(payouts.amount, 0) AS proposal_payouts_hbd_amount,
      COALESCE(hhbd.amount, 0) AS hbdstabilizer_payouts_hbd_amount,
      (COALESCE(phbdout.amount, 0) +
       COALESCE(thbdout.amount, 0) +
       COALESCE(thbd.amount, 0) +
       COALESCE(bhbd.amount, 0) +
       COALESCE(ehbd.amount, 0) - 
       COALESCE(payouts.amount, 0) - 
       COALESCE(hhbd.amount, 0)) AS net_change,
      SUM(COALESCE(phbdout.amount, 0) +
          COALESCE(thbdout.amount, 0) +
          COALESCE(thbd.amount, 0) +
          COALESCE(bhbd.amount, 0) +
          COALESCE(ehbd.amount, 0) - 
          COALESCE(payouts.amount, 0) - 
          COALESCE(hhbd.amount, 0)) 
        OVER(ORDER BY ehbd.day) AS cumulative
      FROM (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_new_emission_allocation'
      ) ehbd
      LEFT JOIN (
      SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
      FROM hive_transfers_daily
      WHERE metric_name = 'DHF_steemit_premine_conversions_hive_in'
      ) phivein
      ON ehbd.day = phivein.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_steemit_premine_conversions_hbd_out'
      ) phbdout
      ON ehbd.day = phbdout.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hive_to_DHF_hive_in'
      ) thivein
      ON ehbd.day = thivein.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hive_to_DHF_hbd_out'
      ) thbdout
      ON ehbd.day = thbdout.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hbd_to_DHF'
      ) thbd
      ON ehbd.day = thbd.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_beneficiary_rewards'
      ) bhbd
      ON ehbd.day = bhbd.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_proposal_payments'
      ) payouts
      ON ehbd.day = payouts.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_hbdstabilizer_payments'
      ) hhbd
      ON ehbd.day = hhbd.day
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`,
      {limitStr: query.limit, offsetStr: query.offset}
      )
    }
    else {
      var [results, metadata] = await this.localConnectionService.executeQuery(await this.formatQuery(`SELECT to_char(ehbd.day, :unitStr) AS unit,
      COALESCE(SUM(phivein.amount), 0) AS premine_hive_in,
      COALESCE(SUM(phbdout.amount), 0) AS premine_hbd_out,
      COALESCE(SUM(thivein.num), 0) AS transfers_hive_num,
      COALESCE(SUM(thivein.amount), 0) AS transfers_hive_amount,
      COALESCE(SUM(thbdout.amount), 0) AS transfers_hive_amount_hbd_out,
      COALESCE(SUM(thbd.num), 0) AS transfers_hbd_num,
      COALESCE(SUM(thbd.amount), 0) AS transfers_hbd_amount,
      COALESCE(SUM(bhbd.num), 0) AS beneficiary_rewards_num,
      COALESCE(SUM(bhbd.amount), 0) AS beneficiary_rewards_hbd_amount,
      COALESCE(SUM(ehbd.amount), 0) AS new_emission_allocation_hbd_amount,
      COALESCE(SUM(payouts.amount), 0) AS proposal_payouts_hbd_amount,
      COALESCE(SUM(hhbd.amount), 0) AS hbdstabilizer_payouts_hbd_amount,
      (COALESCE(SUM(phbdout.amount), 0) +
       COALESCE(SUM(thbdout.amount), 0) +
       COALESCE(SUM(thbd.amount), 0) +
       COALESCE(SUM(bhbd.amount), 0) +
       COALESCE(SUM(ehbd.amount), 0) - 
       COALESCE(SUM(payouts.amount), 0) -
       COALESCE(SUM(hhbd.amount), 0)) AS net_change,
      SUM(COALESCE(SUM(phbdout.amount), 0) +
          COALESCE(SUM(thbdout.amount), 0) +
          COALESCE(SUM(thbd.amount), 0) +
          COALESCE(SUM(bhbd.amount), 0) +
          COALESCE(SUM(ehbd.amount), 0) - 
          COALESCE(SUM(payouts.amount), 0) -
          COALESCE(SUM(hhbd.amount), 0)) 
        OVER(ORDER BY to_char(ehbd.day, :unitStr)) AS cumulative
      FROM (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_new_emission_allocation'
      ) ehbd
      LEFT JOIN (
      SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
      FROM hive_transfers_daily
      WHERE metric_name = 'DHF_steemit_premine_conversions_hive_in'
      ) phivein
      ON ehbd.day = phivein.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_steemit_premine_conversions_hbd_out'
      ) phbdout
      ON ehbd.day = phbdout.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hive_to_DHF_hive_in'
      ) thivein
      ON ehbd.day = thivein.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hive_to_DHF_hbd_out'
      ) thbdout
      ON ehbd.day = thbdout.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'transfers_hbd_to_DHF'
      ) thbd
      ON ehbd.day = thbd.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_beneficiary_rewards'
      ) bhbd
      ON ehbd.day = bhbd.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_proposal_payments'
      ) payouts
      ON ehbd.day = payouts.day
      LEFT JOIN (
        SELECT day, num, TRUNC(amount / 1000.000, 3) AS amount
        FROM hive_transfers_daily
        WHERE metric_name = 'DHF_hbdstabilizer_payments'
      ) hhbd
      ON ehbd.day = hhbd.day
      GROUP BY unit
      ORDER BY unit DESC
      LIMIT :limitStr OFFSET :offsetStr;`, time_unit),
      {unitStr: time_unit, limitStr: query.limit, offsetStr: query.offset}
      )
    }
    
    return results
  }
}
