import { Injectable, Logger, OnModuleInit } from '@nestjs/common'
import { Cron } from '@nestjs/schedule'
import { HiveSqlService } from '../hivesql/hiveSql.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { HiveBeaconService } from '../hivebeacon/hiveBeacon.service'
import { CollectorServiceFunctions } from './collector.service.functions'
import { CollectorServiceTransfers } from './collector.service.transfers'
import { CollectorServiceCommunities } from './collector.service.communities'

@Injectable()
export class CollectorService implements OnModuleInit {
  private readonly logger = new Logger(CollectorService.name)

  private isRunningStartup: boolean = false
  private isRunningHourly: boolean = false
  private isRunningDaily: boolean = false
  private isRunningPeriodically: boolean = false

  constructor(
    private hiveSqlService: HiveSqlService,
    private localConnectionService: LocalConnectionService,
    private hiveBeaconService: HiveBeaconService,
    private collectorServiceFunctions: CollectorServiceFunctions,
    private collectorServiceTransfers: CollectorServiceTransfers,
    private collectorServiceCommunities: CollectorServiceCommunities
    ) {}

  onModuleInit() {
    this.startup()
    //Collect all in case there have been any changes that need stats repopulated
    this.hourly()
    this.daily()
    this.periodically()
  }

  async startup() {
    if (this.isRunningStartup) {
      this.logger.warn('Already collecting STARTUP stats -> skip')
      return
    }

    this.logger.log('Start STARTUP stats snapshot')

    try {
      this.isRunningStartup = true

      const results = await this.hiveSqlService.executeQuery(
        `SELECT *
         FROM comments
         order by created
         OFFSET 10 ROWS
         FETCH NEXT 10 ROWS ONLY;`,
        []
      )

      this.logger.log(`Fetched ${results.length} records`)

    } catch (error) {
      this.logger.error('Error collecting STARTUP stats', error)
    } finally {
      this.isRunningStartup = false
    }

    this.logger.warn('STARTUP stats collected successfully')
  }

  @Cron('0 * * * *')
  async hourly() {
    if (this.isRunningHourly) {
      this.logger.warn('Already collecting HOURLY stats -> skip')
      return
    }

    this.logger.log('Start HOURLY stats snapshot')

    try {
      this.isRunningHourly = true

      // load hourly stats
      // loop and run queries
      // store results
    } catch (error) {
      this.logger.error('Error collecting HOURLY stats', error)
    } finally {
      this.isRunningHourly = false
    }

    this.logger.warn('HOURLY stats collected successfully')
  }

  @Cron('0 */3 * * *')
  async daily() {
    if (this.isRunningDaily) {
      this.logger.warn('Already collecting DAILY stats -> skip')
      return
    }

    this.logger.log('Start DAILY stats snapshot')

    try {
      this.isRunningDaily = true
      
      //Get today's date
      let today = new Date()
      let dd = String(today.getUTCDate()).padStart(2, '0')
      let mm = String(today.getUTCMonth() + 1).padStart(2, '0')
      let yyyy = today.getUTCFullYear()
      let todayString = yyyy + '-' + mm + '-' + dd + " 00:00:00"
      
      //Collect post stats
      await this.collectorServiceFunctions.collectDailyPostStats(todayString)
      
      //Collect comment stats
      await this.collectorServiceFunctions.collectDailyCommentStats(todayString)
      
      //Collect transfer stats
      await this.collectorServiceTransfers.collectDailyTransferStats(todayString)
      
      //Collect Resource Credit stats
      await this.collectorServiceFunctions.collectDailyRcStats(todayString)

      //Collect market stats
      await this.collectorServiceFunctions.collectDailyMarketStats(todayString)

      //Collect communities stats
      await this.collectorServiceCommunities.collectDailyCommunitiesStats(todayString)

      //Collect operation stats
      await this.collectorServiceFunctions.collectDailyOperationStats(todayString)
      
      
    } catch (error) {
      this.logger.error('Error collecting DAILY stats', error)
    } finally {
      this.isRunningDaily = false
    }

    this.logger.warn('DAILY stats collected successfully')
  }
  
  @Cron('30 3 * * *')
  async periodically() {
    if (this.isRunningPeriodically) {
      this.logger.warn('Already collecting PERIODICAL stats -> skip')
      return
    }

    this.logger.log('Start PERIODICAL stats snapshot')

    try {
      this.isRunningPeriodically = true
      
      //Get today's date
      let today = new Date()
      let dd = String(today.getUTCDate()).padStart(2, '0')
      let mm = String(today.getUTCMonth() + 1).padStart(2, '0')
      let yyyy = today.getUTCFullYear()
      let todayString = yyyy + '-' + mm + '-' + dd + " 00:00:00"
      
      //Collect DHF payment stats (beyond daily)
      await this.collectorServiceTransfers.collectDhfPaymentStats(todayString, 'DHF_proposal_payments_weekly')
      await this.collectorServiceTransfers.collectDhfPaymentStats(todayString, 'DHF_proposal_payments_monthly')
      await this.collectorServiceTransfers.collectDhfPaymentStats(todayString, 'DHF_proposal_payments_yearly')
      await this.collectorServiceTransfers.collectDhfPaymentStats(todayString, 'DHF_proposal_payments_lifetime')

      //Collect communities metadata
      await this.collectorServiceCommunities.collectCommunitiesMeta()
      
    } catch (error) {
      this.logger.error('Error collecting PERIODICAL stats', error)
    } finally {
      this.isRunningPeriodically = false
    }

    this.logger.warn('PERIODICAL stats collected successfully')
  }
}
