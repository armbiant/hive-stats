import { Injectable, Logger } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'
import { HiveSqlService } from '../hivesql/hiveSql.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { HiveBeaconService } from '../hivebeacon/hiveBeacon.service'
import { SettingsService } from '../settings/settings.service'

@Injectable()
export class CollectorServiceTransfers {
  private readonly logger = new Logger(CollectorServiceTransfers.name)
  private settings = new SettingsService
  private exchanges = this.settings.exchangeAccountsList()

  constructor(
    @InjectConnection('localConnection')
    private sequelize: Sequelize,
    private hiveSqlService: HiveSqlService,
    private localConnectionService: LocalConnectionService,
    private hiveBeaconService: HiveBeaconService,
  ) {}
  
  
  /*
   * Collect daily transfers stats
   */
  async collectDailyTransferStats(today: string) {
    
    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hive', 'HIVE')
    }
    catch (e) {
      this.logger.error('Error collecting transfers_hive', e)
    }
    
    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hbd', 'HBD')
    } catch (e) {
      this.logger.error('Error collecting transfers_hbd', e)
    }
    
    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hive_from_exchanges', 'HIVE')
    } catch (e) {
      this.logger.error('Error collecting transfers_hive_from_exchanges', e)
    }
    
    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hbd_from_exchanges', 'HBD')
    } catch (e) {
      this.logger.error('Error collecting transfers_hbd_from_exchanges', e)
    }
    
    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hive_to_exchanges', 'HIVE')
    } catch (e) {
      this.logger.error('Error collecting transfers_hive_to_exchanges', e)
    }
    
    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hbd_to_exchanges', 'HBD')
    } catch (e) {
      this.logger.error('Error collecting transfers_hbd_to_exchanges', e)
    }

    /* Individual exchanges */

    for(var exchangeName in this.settings.exchanges) {
      try {
        await this.collectDailyTransferMetrics(today, `transfers_hive_from_exchange__${exchangeName}`, 'HIVE', exchangeName, exchangeName)
      } catch (e) {
        this.logger.error(`Error collecting transfers_hive_from_exchange__${exchangeName}`, e)
      }
  }
    
    for(var exchangeName in this.settings.exchanges) {
        try {
          await this.collectDailyTransferMetrics(today, `transfers_hbd_from_exchange__${exchangeName}`, 'HBD', exchangeName, exchangeName)
        } catch (e) {
          this.logger.error(`Error collecting transfers_hbd_from_exchange__${exchangeName}`, e)
        }
    }

    for(var exchangeName in this.settings.exchanges) {
      try {
        await this.collectDailyTransferMetrics(today, `transfers_hive_to_exchange__${exchangeName}`, 'HIVE', exchangeName, exchangeName)
      } catch (e) {
        this.logger.error(`Error collecting transfers_hive_to_exchange__${exchangeName}`, e)
      }
    }
    
    for(var exchangeName in this.settings.exchanges) {
      try {
        await this.collectDailyTransferMetrics(today, `transfers_hbd_to_exchange__${exchangeName}`, 'HBD', exchangeName, exchangeName)
      } catch (e) {
        this.logger.error(`Error collecting transfers_hbd_to_exchange__${exchangeName}`, e)
      }
    }

    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hive_to_null', 'HIVE')
    } catch (e) {
      this.logger.error('Error collecting transfers_hive_to_null', e)
    }

    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hbd_to_null', 'HBD')
    } catch (e) {
      this.logger.error('Error collecting transfers_hbd_to_null', e)
    }
    
    try {
      await this.collectDailyTransferMetrics(today, 'power_ups', 'HIVE')
    } catch (e) {
      this.logger.error('Error collecting power_ups', e)
    }
    
    try {
      await this.collectDailyPowerDownStats(today, 'power_downs', 'HIVE')
    } catch (e) {
      this.logger.error('Error collecting power_downs', e)
    }
    
    try {
      await this.collectDailySteemitPremineDhfConversions(today)
      //await this.collectDailyHiveTransfersToDhf(today)
      await this.collectDailyTransferMetrics(today, 'transfers_hbd_to_DHF', 'HBD')
      await this.collectDailyDhfBeneficiaryRewards(today, 'DHF_beneficiary_rewards')
      await this.collectDailyDhfAllocationFromNewEmission(today, 'DHF_new_emission_allocation')
      await this.collectDailyDhfPaymentStats(today, 'DHF_proposal_payments')
      await this.collectDailyDhfPaymentStats(today, 'DHF_hbdstabilizer_payments')
    } catch (e) {
      this.logger.error('Error collecting DHF stats', e)
    }
    
    try {
      await this.collectDailyInternalMarketStats(today, 'internal_market_volume')
    } catch (e) {
      this.logger.error('Error collecting internal_market_volume', e)
    }

    try {
      await this.collectDailyCommentRewardStats(today, 'total_comment_rewards_hbd_equiv')
    } catch (e) {
      this.logger.error('Error collecting total_comment_rewards_hbd_equiv', e)
    }

    try {
      await this.collectDailyTransferMetrics(today, 'transfers_hbd_to_savings', 'HBD')
    } catch (e) {
      this.logger.error('Error collecting transfers_hbd_to_savings', e)
    }

    try {
      await this.collectDailyTransfersFromSavings(today, 'transfers_hbd_from_savings', 'HBD')
    } catch (e) {
      this.logger.error('Error collecting transfers_hbd_from_savings', e)
    }

    try {
      await this.collectDailyHbdInterestPayments(today, 'hbd_savings_interest_payments')
    } catch (e) {
      this.logger.error('Error collecting hbd_savings_interest_payments', e)
    }
    
  }
  
  /*
   * Collect daily transfers specific metrics
   */
  async collectDailyTransferMetrics(today: string, metricName: string, token: string, fromEntity = '', toEntity = '') {

    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Set some WHERE conditions
    let transfType = 'transfer'
    let notFrom = ''
    let notTo = ''
    let from = []
    let to = []
    switch(metricName) {
      case 'transfers_hive':
        notFrom = 'NOT'
        from = this.exchanges
        notTo = 'NOT'
        to = to.concat(this.exchanges, 'hive.fund') //Exclude transfers to the DHF. Transfers from the DHF are virtual ops so already excluded from here.
        break
      case 'transfers_hbd':
        notFrom = 'NOT'
        from = this.exchanges
        notTo = 'NOT'
        to = to.concat(this.exchanges, 'hive.fund')
        break
      case 'transfers_hive_from_exchanges':
        notFrom = ''
        from = this.exchanges
        notTo = 'NOT'
        to = this.exchanges
        break
      case 'transfers_hbd_from_exchanges':
        notFrom = ''
        from = this.exchanges
        notTo = 'NOT'
        to = this.exchanges
        break
      case 'transfers_hive_to_exchanges':
        notFrom = 'NOT'
        from = this.exchanges
        notTo = ''
        to = this.exchanges
        break
      case 'transfers_hbd_to_exchanges':
        notFrom = 'NOT'
        from = this.exchanges
        notTo = ''
        to = this.exchanges
        break
      case 'transfers_' + token.toLowerCase() + '_from_exchange__' + fromEntity:
        notFrom = ''
        from = this.settings.exchanges[fromEntity]
        notTo = 'NOT'
        to = this.settings.exchanges[toEntity]
        break
      case 'transfers_' + token.toLowerCase() + '_to_exchange__' + toEntity:
        notFrom = 'NOT'
        from = this.settings.exchanges[fromEntity]
        notTo = ''
        to = this.settings.exchanges[toEntity]
        break
      case 'transfers_hive_to_null':
        notFrom = 'NOT'
        from.push('<') //Impossible value for an account name, so the IN operator will match transfers to any account
        notTo = ''
        to.push('null')
        break
      case 'transfers_hbd_to_null':
        notFrom = 'NOT'
        from.push('<')
        notTo = ''
        to.push('null')
        break
      case 'transfers_hbd_to_DHF':
        notFrom = 'NOT'
        from.push('<')
        notTo = ''
        to = ['steem.dao', 'hive.fund']
        break
      case 'power_ups':
        transfType = 'transfer_to_vesting'
        notFrom = 'NOT'
        from.push('<')
        notTo = 'NOT'
        to.push('<')
        break
      case 'transfers_hbd_to_savings':
        transfType = 'transfer_to_savings'
        notFrom = 'NOT'
        from.push('<')
        notTo = 'NOT'
        to.push('<')
        break
    }
    
    //Num stats
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count
    FROM TxTransfers
    WHERE type = :transfTypeStr
    AND amount_symbol = :tokenStr
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    AND "from" ` + notFrom + ` IN(:fromArr)
    AND "to" ` + notTo + ` IN(:toArr)
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {transfTypeStr: transfType, tokenStr: token, lastDateStr: lastDate, todayStr: today, fromArr: from, toArr: to}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    //Sum stats
    const transfersSum = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, SUM(amount) AS total_amount
    FROM TxTransfers
    WHERE type = :transfTypeStr
    AND amount_symbol = :tokenStr
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    AND "from" ` + notFrom + ` IN(:fromArr)
    AND "to" ` + notTo + ` IN(:toArr)
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {transfTypeStr: transfType, tokenStr: token, lastDateStr: lastDate, todayStr: today, fromArr: from, toArr: to}
    )
    this.logger.log(`Fetched ${transfersSum.length} records`)

    //Combine the num and sum data
    for (let i = 0; i < transfers.length; i++) {
      transfers[i].total_amount = transfersSum[i].total_amount
    }

    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }
  
  /*
   * Collect daily power down stats
   */
  async collectDailyPowerDownStats(today: string, metricName: string, token: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Num stats
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count
    FROM VOFillVestingWithdraws
    WHERE deposited_symbol = :tokenStr
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {tokenStr: token, lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    //Sum stats
    const transfersSum = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, SUM(deposited) AS total_amount
    FROM VOFillVestingWithdraws
    WHERE deposited_symbol = :tokenStr
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {tokenStr: token, lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfersSum.length} records`)
    
    //Combine the num and sum data
    for (let i = 0; i < transfers.length; i++) {
      transfers[i].total_amount = transfersSum[i].total_amount
    }

    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily conversions of the Steemit Inc. pre-mined stake to HBD for the community treasury (initially called Steem Proposal System - SPS, and later called Decentralized Hive Fund - DHF)
   */
  async collectDailySteemitPremineDhfConversions(today: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = 'DHF_steemit_premine_conversions_hive_in'
      ORDER BY day DESC
      LIMIT 1;`,
      []
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //For now, HiveSQL has two separate tables for the SPS and DHF periods
    //Furthermore, in VODHFConversions (and maybe in the blockchain itself) there is no differentiation between HIVE transfers coming in and immediately being converted to HBD, and the pre-mined Steemit stake being converted to HBD. So we use the timestamp of the regular daily pre-mine conversions, although this gives some small errors when other transfers are made at exactly that time
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, 'SPS' AS type, COUNT(*) AS count, SUM(hive_amount_in) AS hive_amount_in, SUM(hbd_amount_out) AS hbd_amount_out
    FROM VOSPSConverts
    WHERE timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    UNION ALL
    SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, 'DHF', COUNT(*) AS count, SUM(hive_amount_in) AS hive_amount_in, SUM(hbd_amount_out) AS hbd_amount_out
    FROM VODHFConversions
    WHERE CONVERT(TIME,timestamp) = '19:31:30'
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, 'DHF_steemit_premine_conversions_hive_in', transfer.count, (transfer.hive_amount_in * 1000)],
        tr)

        await this.localConnectionService.executeTranQuery(
          `INSERT INTO hive_transfers_daily (day, metric_name, amount)
          VALUES (make_date(?, ?, ?), ?, ?);`,
          [transfer.year, transfer.month, transfer.day, 'DHF_steemit_premine_conversions_hbd_out', (transfer.hbd_amount_out * 1000)],
          tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily transfers of HIVE to the DHF. These transfers are immediately converted to HBD.
   */
  async collectDailyHiveTransfersToDhf(today: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = 'transfers_hive_to_DHF_hive_in'
      ORDER BY day DESC
      LIMIT 1;`,
      []
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(hive_amount_in) AS hive_amount_in, SUM(hbd_amount_out) AS hbd_amount_out
    FROM VODHFConversions
    WHERE CONVERT(TIME,timestamp) <> '19:31:30'
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, 'transfers_hive_to_DHF_hive_in', transfer.count, (transfer.hive_amount_in * 1000)],
        tr)

        await this.localConnectionService.executeTranQuery(
          `INSERT INTO hive_transfers_daily (day, metric_name, amount)
          VALUES (make_date(?, ?, ?), ?, ?);`,
          [transfer.year, transfer.month, transfer.day, 'transfers_hive_to_DHF_hbd_out', (transfer.hbd_amount_out * 1000)],
          tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily post/comment beneficiary rewards to the DHF. These rewards are paid to the DHF only in HBD.
   */
  async collectDailyDhfBeneficiaryRewards(today: string, metricName: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(hbd_payout) AS total_amount
    FROM VOCommentBenefactorRewards
    WHERE benefactor IN('steem.dao', 'hive.fund')
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect the daily allocation to the DHF from the newly-minted tokens that form the Hive reward pool.
   */
  async collectDailyDhfAllocationFromNewEmission(today: string, metricName: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(additional_funds) AS total_amount
    FROM(
    SELECT timestamp, additional_funds
    FROM VOSPSFunds
    WHERE timestamp > :lastDateStr
    AND timestamp < :todayStr
    UNION ALL
    SELECT timestamp, additional_funds
    FROM VODHFFundings
    WHERE timestamp > :lastDateStr
    AND timestamp < :todayStr
    ) AS u
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily DHF payments to proposals stats
   */
  async collectDailyDhfPaymentStats(today: string, metricName: string) {
    
    //Excluded accounts
    let accounts = ['steem.dao']
    if(metricName == 'DHF_proposal_payments') accounts.push('hbdstabilizer')

    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Num & sum stats (smaller table so combined both in one query)
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(DISTINCT(proposal_id)) AS count, SUM(payment) as total_amount
    FROM VOProposalPays
    WHERE receiver NOT IN(:accountsArr)
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {accountsArr: accounts, lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect DHF payments to proposals stats (weekly, monthly, yearly, lifetime)
   * Necessary for counting the unique proposals funded for the time period
   */
  async collectDhfPaymentStats(today: string, metricName: string) {
    
    /* We don't know if any of the stats for the previous time units were completely or only partially filled. E.g. if the last collection happened in the middle of last week or last month or last year, we will have only partial stats for that period. So we delete all we have and populate from scratch. */
    
    let query = ''
    switch(metricName) {
      case 'DHF_proposal_payments_weekly':
        //Collect in ISO weeks since that's how we provide it in the API
        //Solution for getting ISO years taken from https://capens.net/content/sql-year-iso-week
        query = `SELECT YEAR(DATEADD(day, 26 - DATEPART(isoww, timestamp), timestamp)) AS year, 
        DATEPART(isoww, timestamp) AS week, 
        CONCAT_WS('-', 
          YEAR(DATEADD(day, 26 - DATEPART(isoww, timestamp), timestamp)), 
          FORMAT(DATEPART(isoww, timestamp), '00')) 
          AS time_unit, 
        COUNT(DISTINCT(proposal_id)) AS count, 
        SUM(payment) as total_amount
        FROM VOProposalPays
        WHERE receiver NOT IN('steem.dao','hbdstabilizer')
        GROUP BY 
          YEAR(DATEADD(day, 26 - DATEPART(isoww, timestamp), timestamp)), 
          DATEPART(isoww, timestamp)
        ORDER BY year, week ASC;`
        break
      case 'DHF_proposal_payments_monthly':
        query = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, CONCAT_WS('-', YEAR(timestamp), FORMAT(MONTH(timestamp), '00')) AS time_unit, COUNT(DISTINCT(proposal_id)) AS count, SUM(payment) AS total_amount
        FROM VOProposalPays
        WHERE receiver NOT IN('steem.dao','hbdstabilizer')
        GROUP BY YEAR(timestamp), MONTH(timestamp)
        ORDER BY year, month ASC;`
        break
      case 'DHF_proposal_payments_yearly':
        query = `SELECT YEAR(timestamp) AS year, YEAR(timestamp) AS time_unit, COUNT(DISTINCT(proposal_id)) AS count, SUM(payment) AS total_amount
        FROM VOProposalPays
        WHERE receiver NOT IN('steem.dao','hbdstabilizer')
        GROUP BY YEAR(timestamp)
        ORDER BY year ASC;`
        break
      case 'DHF_proposal_payments_lifetime':
        query = `SELECT 'AD' AS time_unit, COUNT(DISTINCT(proposal_id)) AS count, SUM(payment) AS total_amount
        FROM VOProposalPays
        WHERE receiver NOT IN('steem.dao','hbdstabilizer');`
        break
    }
    
    const transfers = await this.hiveSqlService.executeQuery(query, {})
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      await this.localConnectionService.executeQuery(
      `DELETE FROM hive_transfers
      WHERE metric_name = ?`,
      [metricName]
      )
      
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers (time_unit, metric_name, num, amount)
        VALUES (?, ?, ?, ?);`,
        [transfer.time_unit, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }
  
  /*
   * Collect daily internal market stats
   */
  async collectDailyInternalMarketStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Num & sum stats (smaller table so combined both in one query)
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(
    CASE
      WHEN current_pays_symbol = 'HBD' THEN current_pays
      WHEN open_pays_symbol = 'HBD' THEN open_pays
    END) AS total_amount
    FROM VOFillOrders
    WHERE timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily comment reward stats
   */
  async collectDailyCommentRewardStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Num & sum stats (smaller table so combined both in one query)
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(payout) AS total_hbd
    FROM VOCommentRewards
    WHERE timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.total_hbd * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily transfers from savings
   */
  async collectDailyTransfersFromSavings(today: string, metricName: string, token: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Num & sum stats (smaller table so combined both in one query)
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(amount) AS amount
    from VOFillTransferFromSavings
    WHERE amount_symbol = :tokenStr
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {tokenStr: token, lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily interest payments on HBD in Savings
   */
  async collectDailyHbdInterestPayments(today: string, metricName: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_transfers_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since after HF 25 when interest started being paid only when HBD is held in Savings: https://gitlab.syncad.com/hive/hive/-/releases/v1.25.0
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "2021-06-30 14:00:00"
    
    //Num & sum stats (smaller table so combined both in one query)
    const transfers = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS count, SUM(interest) AS amount
    from VOInterests
    WHERE timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${transfers.length} records`)
    
    const tr = await this.sequelize.transaction();
    
    try {
      for (const transfer of transfers) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_transfers_daily (day, metric_name, num, amount)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [transfer.year, transfer.month, transfer.day, metricName, transfer.count, (transfer.amount * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

}
