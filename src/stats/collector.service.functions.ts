import { Injectable, Logger } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'
import { HiveSqlService } from '../hivesql/hiveSql.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { HiveBeaconService } from '../hivebeacon/hiveBeacon.service'
import { SettingsService } from '../settings/settings.service'

@Injectable()
export class CollectorServiceFunctions {
  private readonly logger = new Logger(CollectorServiceFunctions.name)
  private settings = new SettingsService
  private exchanges = this.settings.exchanges

  constructor(
    @InjectConnection('localConnection')
    private sequelize: Sequelize,
    private hiveSqlService: HiveSqlService,
    private localConnectionService: LocalConnectionService,
    private hiveBeaconService: HiveBeaconService,
  ) {}
  
  
  /*
   * Collect daily post stats
   */
  async collectDailyPostStats(today: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_comments_daily
      WHERE type = 'posts'
      ORDER BY day DESC
      LIMIT 1;`,
      []
    )
    
    //No stats yet in our database - populate from scratch
    if(localStats.length == 0) {
      const posts = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth = 0
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [today]
      )
      this.logger.log(`Fetched ${posts.length} records`)
      
      posts.forEach(async (post) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'posts', ?);`,
        [post.year, post.month, post.day, post.count])
      })
    }
    //We have stats in our database
    else {
      //Get only post stats after last fetched date and before today
      let lastDate = localStats[0].day + " 23:59:59"
      
      const posts = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth = 0
      AND created > ?
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [lastDate, today]
      )
      this.logger.log(`Fetched ${posts.length} records`)
      
      //Add new post stats for missing days, if any
      posts.forEach(async (post) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'posts', ?);`,
        [post.year, post.month, post.day, post.count])
      })
    }
    
    return
  }
  
  
  /*
   * Collect daily comment stats
   */
  async collectDailyCommentStats(today: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_comments_daily
      WHERE type = 'comments'
      ORDER BY day DESC
      LIMIT 1;`,
      []
    )

    //No stats yet in our database - populate from scratch
    if(localStats.length == 0) {
      const comments = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth <> 0
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [today]
      )
      this.logger.log(`Fetched ${comments.length} records`)
      
      comments.forEach(async (comment) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'comments', ?);`,
        [comment.year, comment.month, comment.day, comment.count])
      })
    }
    //We have stats in our database
    else {
      //Get only comment stats after last fetched date and before today
      let lastDate = localStats[0].day + " 23:59:59"
      
      const comments = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth <> 0
      AND created > ?
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [lastDate, today]
      )
      this.logger.log(`Fetched ${comments.length} records`)
      
      //Add new comment stats for missing days, if any
      comments.forEach(async (comment) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'comments', ?);`,
        [comment.year, comment.month, comment.day, comment.count])
      })
    }
    
    return
  }
  
  /*
   * Collect daily operations stats
   */
  async collectDailyOperationStats(today: string) {

    //custom_json stats
    for(var app of this.settings.trackedApps) {
      for(var appName in app) {
        try {
          await this.collectDailyCustomJsonMetrics(today, appName, app[appName])
        }
        catch (e) {
          this.logger.error(`Error collecting ${appName} custom_jsons`, e)
        }
      }
    }
  }
  
  /*
   * Collect daily Resource Credit stats
   */
  async collectDailyRcStats(today: string) {
    
    //Turn timestamp to just date
    today = today.substr(0, 10)
    
    //Check if we have collected RC stats today
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_rc_daily
      WHERE metric_name = 'rc_cost_vote_operation'
      AND day = ?;`,
      [today]
    )

    //If not, collect RC stats for the day
    if(localStats.length == 0) {
      const rcStats = await this.hiveBeaconService.getRcCosts()
      
      const tr = await this.sequelize.transaction();
      
      try {
        for (const rcStat of rcStats) {
          let rcMetricName = 'rc_cost_' + rcStat.operation
          await this.localConnectionService.executeTranQuery(
          `INSERT INTO hive_rc_daily (day, metric_name, value)
          VALUES (?, ?, ?);`,
          [today, rcMetricName, rcStat.rc_needed],
          tr)

          let hpMetricName = 'hp_equiv_' + rcStat.operation
          await this.localConnectionService.executeTranQuery(
          `INSERT INTO hive_rc_daily (day, metric_name, value)
          VALUES (?, ?, ?);`,
          [today, hpMetricName, (rcStat.hp_needed * 10000)],
          tr)
        }
        await tr.commit()
      } catch (e) {
        this.logger.error('Error:', e)
        await tr.rollback()
      }
    }
    
    return
  }

  /*
   * Collect daily market stats
   */
  async collectDailyMarketStats(today: string) {
    
    try {
      await this.collectDailyTotalSupplyMetrics(today)
    }
    catch (e) {
      this.logger.error('Error collecting total_supply_metrics_hive', e)
    }

  }

  /*
   * Collect daily custom_json metrics
   */
  async collectDailyCustomJsonMetrics(today: string, appName: string, assocCustomJsons: string) {
    
    
    //Load the daily stats we already have
    if(appName !== "") {
      var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
        `SELECT day
        FROM hive_operations_daily
        WHERE app = ?
        ORDER BY day DESC
        LIMIT 1;`,
        [appName]
      )
    }
    //else { //Hive core custom_jsons
    //  var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
    //    `SELECT day
    //    FROM hive_operations_daily
    //    WHERE metric_name = ?
    //    ORDER BY day DESC
    //    LIMIT 1;`,
    //    [this.settings[assocCustomJsons][0]]
    //  )
    //}
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Max of 30 custom_jsons, to keep response time workable
    let customJsons = JSON.parse(JSON.stringify(this.settings[assocCustomJsons])) //This makes a copy so that the original will not be affected when we splice later
    let iterations = Math.ceil(customJsons.length / 30)
    let operationsAll = []
    
    for(var i = 1; i <= iterations; i++) {
      let customJsonsBatch = customJsons.splice(0, 30)
      
      var operations = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, tid, COUNT(*) AS count
      FROM TxCustoms
      WHERE tid IN(:customJsonIds)
      AND timestamp > :lastDateStr
      AND timestamp < :todayStr
      GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp), tid
      ORDER BY year, month, day ASC;`,
      {customJsonIds: customJsonsBatch, lastDateStr: lastDate, todayStr: today}
      )
      this.logger.log(`Fetched ${operations.length} records`)

      operationsAll = operationsAll.concat(operations)
    }

    const tr = await this.sequelize.transaction();
    
    try {
      for (const operation of operationsAll) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_operations_daily (day, metric_name, app, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [operation.year, operation.month, operation.day, operation.tid, appName, operation.count],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily total supply metrics
   */
  async collectDailyTotalSupplyMetrics(today: string) {
    
    //Check if we have collected supply stats today
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_market_daily
      WHERE metric_name = ?
      AND day = ?;`,
      ['total_hive_supply', today]
    )

    //If not, collect total supply stats for the day
    if(localStats.length == 0) {
      
      let supply = await this.hiveSqlService.executeQuery(
      `SELECT virtual_supply, current_supply, current_hbd_supply
      FROM DynamicGlobalProperties;`,
      {}
      )
      this.logger.log(`Fetched ${supply.length} records`)
      
      const tr = await this.sequelize.transaction();

      try {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_market_daily (day, metric_name, value)
        VALUES (?, ?, ?);`,
        [today, 'total_virtual_supply', supply[0].virtual_supply],
        tr)

        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_market_daily (day, metric_name, value)
        VALUES (?, ?, ?);`,
        [today, 'total_hive_supply', supply[0].current_supply],
        tr)

        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_market_daily (day, metric_name, value)
        VALUES (?, ?, ?);`,
        [today, 'total_hbd_supply', supply[0].current_hbd_supply],
        tr)
        await tr.commit()
      } catch (e) {
        this.logger.error('Error:', e)
        await tr.rollback()
      }
    }
    
    return
  }

}
