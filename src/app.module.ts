import { ConfigModule } from '@nestjs/config'
import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { CollectorModule } from './stats/collector.module'
import { HiveSqlModule } from './hivesql/hiveSql.module'
import { LocalConnectionModule } from './localconnection/localConnection.module'
import { StatsModule } from './stats/stats.module'
import { HiveBeaconModule } from './hivebeacon/hiveBeacon.module'
import { SequelizeModule } from '@nestjs/sequelize'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.dev', '.env']
    }),
    ScheduleModule.forRoot(),
    SequelizeModule.forRoot({
      dialect: 'mssql',
      uri: process.env.HIVESQL_DATABASE_URL,
      synchronize: true,
      name: 'hivesqlConnection',
      dialectOptions: {
        options: { "requestTimeout": 9000000 }
      },
      models: []
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      uri: process.env.DATABASE_URL,
      synchronize: true,
      models: [],
      name: 'localConnection',
      dialectOptions: {
        ssl: {
          require: true,
          rejectUnauthorized: false
        }
      },
      pool: {
        max: 30,
        min: 0,
        acquire: 60000,
        idle: 5000
      }
    }),
    HiveSqlModule,
    CollectorModule,
    LocalConnectionModule,
    StatsModule,
    HiveBeaconModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
