import { Module } from '@nestjs/common'
import { LocalConnectionService } from './localConnection.service'

@Module({
  imports: [],
  controllers: [],
  providers: [LocalConnectionService],
  exports: [LocalConnectionService]
})
export class LocalConnectionModule {}
