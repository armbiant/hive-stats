import { Injectable } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'

@Injectable()
export class LocalConnectionService {
  constructor(
    @InjectConnection('localConnection')
    private sequelize: Sequelize
  ) {}

  async executeQuery(query: string, params: any): Promise<any[]> {
    return await this.sequelize.query(query, {
      replacements: params
    })
  }
  
  /*
   * Execute a query that is a part of a transaction
   */
  async executeTranQuery(query: string, params: any[], tr: any): Promise<any[]> {
    return await this.sequelize.query(query, {
      replacements: params,
      transaction: tr
    })
  }
}
